package TestCases;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import excelExportAndFileIO.ReadExcelFile;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import operation.UIOperation;

//==============================================START_CLASS==================================================================================

public class BaseClass1 {
	public AndroidDriver<WebElement> driver = null;
	WebDriver driver1 = null;
	public String Strarray[];
	int sheetIndex = 0;
	int index = 0;
	private String deviceVersion;
	private String deviceName;
	private String adbLocation;

	BaseClass1() {

	}

	// ============================================READ
	// TESTCASES_TO_EXECUTE=======================================================
	@Test(dataProvider = "DP1", priority = 1)
	public void readTestCasesToExecute(String Testname, String Enabled) throws Exception {
		getDataFromDataprovider1();
		if (Testname != null && Enabled.contains("Y")) {
			readDeviceInformation("DeviceInformation");
			readDataFromSheet(Testname);
		}
	}

	// ============================================END_READ
	// TESTCASES_TO_EXECUTE===================================================

	// =====================================================START_SCREENSHOT==================================================================================
	@AfterMethod
	public void saveScreenshot(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			Utility.captureScreenshot(driver, result.getInstanceName());
			// driver.resetApp();
			driver.closeApp();
		}
		// driver.close();
	}
	// =======================================================END_SCREENSHOT================================================================================

	// ======================================================START_TestCase_Steps===========================================================================

	private void readDataFromSheet(String sheetName) throws Exception {
		ReadExcelFile file = new ReadExcelFile();
		String testcaseName, keyword, objectType, objectValue, value;
		DataFormatter formatter = new DataFormatter();

		// Read keyword sheet
		Sheet Sheet1 = (Sheet) file.readExcel(System.getProperty("user.dir") + "\\", "TestCaseSteps.xlsx", sheetName);
		this.sheetIndex++;

		// Find number of rows in excel file
		int rowCount = ((org.apache.poi.ss.usermodel.Sheet) Sheet1).getLastRowNum()
				- ((org.apache.poi.ss.usermodel.Sheet) Sheet1).getFirstRowNum();

		for (int i = 0; i < rowCount; i++) {
			// Loop over all the rows
			Row row = ((org.apache.poi.ss.usermodel.Sheet) Sheet1).getRow(i + 1);
			// Create a loop to print cell values in a row
			testcaseName = row.getCell(0).getStringCellValue();
			keyword = row.getCell(1).getStringCellValue();
			objectType = row.getCell(3).getStringCellValue();
			objectValue = row.getCell(4).getStringCellValue();
			if (row.getCell(5).getCellType() == Cell.CELL_TYPE_NUMERIC) {
				value = String.valueOf(row.getCell(5).getNumericCellValue());
			} else {
				value = String.valueOf(row.getCell(5).getStringCellValue());
			}
			System.out.println("Object Name : " + row.getCell(2).getStringCellValue() + " : " + value);
			this.transaction(testcaseName, keyword, objectType, objectValue, value);
		}
	}

	private void readDeviceInformation(String sheetName) throws Exception {
		ReadExcelFile file = new ReadExcelFile();
		// Read keyword sheet
		Sheet Sheet1 = (Sheet) file.readExcel(System.getProperty("user.dir") + "\\", "TestCaseSteps.xlsx", sheetName);
		this.sheetIndex++;

		// Find number of rows in excel file
		int rowCount = ((org.apache.poi.ss.usermodel.Sheet) Sheet1).getLastRowNum()
				- ((org.apache.poi.ss.usermodel.Sheet) Sheet1).getFirstRowNum();

		// for (int i = 0; i < rowCount; i++)
		// {
		// Loop over all the rows
		Row row = ((org.apache.poi.ss.usermodel.Sheet) Sheet1).getRow(1);
		// Create a loop to print cell values in a row
		deviceName = row.getCell(0).getStringCellValue();
		deviceVersion = row.getCell(1).getStringCellValue();
		adbLocation = row.getCell(2).getStringCellValue();
		// }
		System.out.println("Device name : " + deviceName + " Version : " + deviceVersion);
	}

	// =======================================================END_TestCase_Steps===========================================================================

	// =====================================================START_OPERATION=========================================================================

	public void transaction(String testcaseName, String keyword, String objectType, String objectValue, String value)
			throws Exception {
		try {
			if (testcaseName != null && testcaseName.length() != 0) {
				System.setProperty("webdriver.chrome.driver",
						(System.getProperty("user.dir") + "\\" + "chromedriver.exe"));
				// driver1 = new ChromeDriver();

				// Created object of DesiredCapabilities class.
				DesiredCapabilities capabilities = new DesiredCapabilities();
				capabilities.setCapability("deviceName", deviceName);
				capabilities.setCapability(CapabilityType.BROWSER_NAME, "Android");
				capabilities.setCapability(CapabilityType.VERSION, deviceVersion);
				capabilities.setCapability("platformName", "Android");
				capabilities.setCapability("appPackage", "com.transfast.transfast.debug");
				capabilities.setCapability("appActivity", "com.transfast.transfast.ui.login.StartActivity");
				capabilities.setCapability("unicodekeyboard", true);
				capabilities.setCapability("resetkeyboard", true);
				driver = new AndroidDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			}
			// ReadObject object = new ReadObject();
			// Properties allObjects = object.getObjectRepository();
			UIOperation operation = new UIOperation(driver);
			operation.perform(keyword, objectType, objectValue, value, adbLocation);
		} catch (AssertionError e) {
			System.out.println("Inside Transaction Exception");
			System.out.println("Hi Vinit Exception in test case execution: " + e.getMessage());
			Assert.fail();
		} // end catch
	}// end method

	// =====================================================END_OPERATION=========================================================================

	// =================================================START_DATA_PROVIDER=======================================================================================
	@DataProvider(name = "DP1")
	public Object[][] getDataFromDataprovider1() throws Exception {
		Object[][] object1 = null;
		ReadExcelFile file = new ReadExcelFile();

		// Read keyword sheet
		Sheet Sheet1 = (Sheet) file.readExcel(System.getProperty("user.dir") + "\\", "TestCaseSteps.xlsx",
				"ExecuteState");
		// Find number of rows in excel file
		int rowCount = ((org.apache.poi.ss.usermodel.Sheet) Sheet1).getLastRowNum()
				- ((org.apache.poi.ss.usermodel.Sheet) Sheet1).getFirstRowNum();
		this.Strarray = new String[rowCount];
		object1 = new Object[rowCount][2];
		for (int i = 0; i < rowCount; i++) {
			// Loop over all the rows
			Row row = ((org.apache.poi.ss.usermodel.Sheet) Sheet1).getRow(i + 1);
			// Create a loop to print cell values in a row
			for (int j = 0; j < row.getLastCellNum(); j++) {
				// Print excel data in console
				object1[i][j] = row.getCell(j).getStringCellValue();
			}
		}
		return object1;
	}

	// @DataProvider(name="DP2")
	// public Object[][] getDataFromDataprovider2() throws Exception
	// {
	// Object[][] object1 = null;
	// ReadExcelFile file = new ReadExcelFile();
	//
	// //Read keyword sheet
	// Sheet Sheet1 = (Sheet)
	// file.readExcel(System.getProperty("user.dir")+"\\","TestCaseSteps.xlsx" ,
	// "DeviceInformation" );
	// //Find number of rows in excel file
	// int rowCount = ((org.apache.poi.ss.usermodel.Sheet)
	// Sheet1).getLastRowNum()-((org.apache.poi.ss.usermodel.Sheet)
	// Sheet1).getFirstRowNum();
	// this.Strarray=new String [rowCount];
	// object1 = new Object[rowCount][2];
	// for (int i = 0; i < rowCount; i++)
	// {
	// //Loop over all the rows
	// Row row = ((org.apache.poi.ss.usermodel.Sheet) Sheet1).getRow(i+1);
	// //Create a loop to print cell values in a row
	// for (int j = 0; j < row.getLastCellNum(); j++)
	// {
	// object1[i][j] = row.getCell(j).getStringCellValue();
	// }
	// }
	// return object1;
	// }
	// =================================================END_DATA_PROVIDER=======================================================================================

	// =====================================================MAIN_CLASS=========================================================================

	public static void main(String[] args) {
		// BaseClass1 baseClass1 = new BaseClass1();
		try {
			// baseClass1.readDeviceInformation("DeviceInformation");
			System.out.println("Inside Main class");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	// =================================================END MAIN
	// CLASS=======================================================================================

}
// ===================================================END_CLASS=============================================================================================
