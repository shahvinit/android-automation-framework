package operation;

import java.net.URL;

import org.apache.http.util.TextUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

public class UIOperation {

	AndroidDriver<WebElement> driver;
	WebDriverWait wait;
	Dimension size;

	Dimension dimensions;
	Double screenHeightStart;
	int scrollStart;
	Double screenHeightEnd;
	int scrollEnd;
	By objectResult;

	public UIOperation(AndroidDriver<WebElement> driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 120);
	}

	public void perform(String operation, String objectType, String objectValue, String value, String adbLocation)
			throws Exception {

		if (!TextUtils.isEmpty(objectType) && !TextUtils.isEmpty(objectValue)) {
			objectResult = this.getObject(objectType, objectValue);
		}

		switch (operation.toUpperCase()) {

		case "CLICK":
			// Perform click
			// wait.until(ExpectedConditions.elementToBeClickable(this.getObject(objectType,objectValue)));
			// System.out.println("Click : "+objectName +" : "+objectType);
			driver.findElement(objectResult).click();
			// System.out.println("Click");
			break;

		case "CLOSE":
			// Close browser
			Thread.sleep(2500);
			driver.close();
			break;

		case "SCROLL2OTHERS":
			dimensions = driver.manage().window().getSize();
			screenHeightStart = dimensions.getHeight() * 0.9;
			scrollStart = screenHeightStart.intValue();
			screenHeightEnd = dimensions.getHeight() * 0.1;
			scrollEnd = screenHeightEnd.intValue();
			for (int i = 0; i < 5; i++) {
				driver.swipe(0, scrollStart, 0, scrollEnd, 0);
				if (i == 3) {
					if (driver.findElements(By.name("Other")).size() != 0)
						break;
				}
			}

			// while(driver.findElements(By.name("Other")).size()==0)
			// {
			// Dimension dimensions = driver.manage().window().getSize();
			// Double screenHeightStart = dimensions.getHeight() * 0.9;
			// int scrollStart = screenHeightStart.intValue();
			// Double screenHeightEnd = dimensions.getHeight() * 0.1;
			// int scrollEnd = screenHeightEnd.intValue();
			// driver.swipe(0,scrollStart,0,scrollEnd,0);
			// }
			break;

		case "SCROLL2CONTINUE":
			dimensions = driver.manage().window().getSize();
			screenHeightStart = dimensions.getHeight() * 0.9;
			scrollStart = screenHeightStart.intValue();
			screenHeightEnd = dimensions.getHeight() * 0.1;
			scrollEnd = screenHeightEnd.intValue();
			for (int i = 0; i < 5; i++) {
				driver.swipe(0, scrollStart, 0, scrollEnd, 0);
				// if(i == 3)
				// {
				if (driver.findElements(By.name("Continue")).size() != 0) {
					break;
				}
				// }
			}

			// while(driver.findElements(By.name("Continue")).size()==0)
			// {
			// dimensions = driver.manage().window().getSize();
			// screenHeightStart = dimensions.getHeight() * 0.9;
			// scrollStart = screenHeightStart.intValue();
			// screenHeightEnd = dimensions.getHeight() * 0.1;
			// scrollEnd = screenHeightEnd.intValue();
			// driver.swipe(0,scrollStart,0,scrollEnd,0);
			// }

			break;
		case "SCROLL_DOWN":
			dimensions = driver.manage().window().getSize();
			screenHeightStart = dimensions.getHeight() * 0.5;
			scrollStart = screenHeightStart.intValue();
			screenHeightEnd = dimensions.getHeight() * 0.2;
			scrollEnd = screenHeightEnd.intValue();
			driver.swipe(0, scrollStart, 0, scrollEnd, 0);
			break;

		case "SCROLL_CALENDAR":
			for (int i = 0; i < 3; i++) {
				driver.swipe(548, 873, 548, 1500, 0);				
			}
			break;

		case "SETTEXT": // Set text on control
			MobileElement element = (MobileElement) driver.findElement(objectResult);
			// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			wait.until(ExpectedConditions.elementToBeClickable(objectResult));
			// driver.findElement(objectResult).click();
			if (element.getText().length() > 0) {
				driver.findElement(objectResult).clear();
			}
			// driver.findElement(objectResult).sendKeys(value);
			value = value.replaceAll(" ", "%s");
			Runtime.getRuntime().exec(adbLocation + " shell input text " + value);

			// Thread.sleep(2000);
			break;

		case "CLEAR":
			// Perform click
			wait.until(ExpectedConditions.elementToBeClickable(objectResult));
			driver.findElement(objectResult).clear();
			break;

		// case "DOUBLE_CLICK":
		// //Perform Double click
		// wait.until(ExpectedConditions.elementToBeClickable(this.getObject(p,objectName,objectType)));
		// WebElement ele =
		// driver.findElement(this.getObject(p,objectName,objectType));
		// Actions action1 = new Actions(driver);
		// action1.doubleClick(ele).perform();
		// System.out.println("Double Click");
		// break;

		// case "SWITCHTOIFRAME":
		// Thread.sleep(2000);
		// WebElement iframeSwitch =
		// driver.findElement(By.id("plaid-link-iframe"));
		// driver.switchTo().frame(iframeSwitch);
		// System.out.println("switcehd to iFrame");
		// break;

		// case "SWITCHTODEFAULT":
		// //driver.findElement(this.getObject(p,objectName,objectType)).click();
		// driver.switchTo().defaultContent();
		// System.out.println("switcehd to default");
		// break;

		// case "SELECTDROPDOWN":
		// //Select value from drop down
		// //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// wait.until(ExpectedConditions.elementToBeClickable(this.getObject(p,objectName,objectType)));
		// driver.findElement(this.getObject(p, objectName,
		// objectType)).sendKeys(value);
		// driver.findElement(this.getObject(p, objectName,
		// objectType)).sendKeys(Keys.ARROW_DOWN);
		// driver.findElement(this.getObject(p, objectName,
		// objectType)).sendKeys(Keys.ENTER);
		// //Thread.sleep(3000);
		// break;

		case "GETKEYPAD":
			driver.getKeyboard();
			break;

		case "HIDEKEYPAD":
			hideKeypad();
			break;

		// case "ESCAPE":
		// //Enter Escape button
		// Actions action = new Actions(driver);
		// action.sendKeys(Keys.ESCAPE).perform();

		default:
			break;

		case "WAIT_2_SECONDS":
			Thread.sleep(2000);
			break;
		case "WAIT_3_SECONDS":
			Thread.sleep(3000);
			break;
		case "WAIT_4_SECONDS":
			Thread.sleep(4000);
			break;
		case "WAIT_5_SECONDS":
			Thread.sleep(5000);
			break;
		case "WAIT_6_SECONDS":
			Thread.sleep(6000);
			break;
		case "WAIT_7_SECONDS":
			Thread.sleep(7000);
			break;
		case "ASSERTION":
			wait.until(ExpectedConditions.elementToBeClickable(this.getObject(objectType, objectValue)));
			WebElement a = driver.findElement(this.getObject(objectType, objectValue));
			try {
				Assert.assertEquals(value, a.getText(), "Test Case Failed");
			} catch (AssertionError e) {
				Reporter.log("Error: " + e.getLocalizedMessage());
				Assert.fail();
			}
			break;
		// case "EXPLICIT_WAIT":
		// //Explicit wait
		// WebDriverWait wait = new WebDriverWait(driver, 10);
		// wait.until(ExpectedConditions.elementToBeClickable(By.xpath(objectName)));
		// System.out.println("test");
		// break;

		}
	}

	/**
	 * Find element BY using object type and value
	 * 
	 * @param p
	 * @param objectName
	 * @param objectType
	 * @return
	 * @throws Exception
	 */
	private By getObject(String objectType, String objectValue) throws Exception {
		// Find by xpath
		if (objectType.equalsIgnoreCase("XPATH")) {

			return By.xpath(objectValue);
		}

		// Find by id
		else if (objectType.equalsIgnoreCase("ID")) {

			return By.id(objectValue);
		}

		// find by class
		else if (objectType.equalsIgnoreCase("CLASSNAME")) {

			return By.className(objectValue);

		}
		// find by name
		else if (objectType.equalsIgnoreCase("NAME")) {

			return By.name(objectValue);

		}
		// Find by css
		else if (objectType.equalsIgnoreCase("CSS")) {

			return By.cssSelector(objectValue);

		}
		// find by link
		else if (objectType.equalsIgnoreCase("LINK")) {

			return By.linkText(objectValue);

		}
		// find by partial link
		else if (objectType.equalsIgnoreCase("PARTIALLINK")) {

			return By.partialLinkText(objectValue);

		} else {
			throw new Exception("Wrong object type");
		}
	}

	private void hideKeypad() {
		try {
			if (driver.getKeyboard() != null) {
				driver.hideKeyboard();
			}
		} catch (Exception e) {
			// driver.getKeyboard();
		}
	}
}
